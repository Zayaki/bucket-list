<?php

namespace App\Utils;

class Censurator
{
    public function purify($message){
        $wordsToCheck = array(
            "poop" ,
            "nope",
            "enfoiré",
            "bite" ,
            "baisable",
            "baise",
            "baiser" ,
            "bander" ,
            "branler" ,
            "branlette" ,
            "bordel" ,
            "burnes",
            "con",
            "conne",
            "connard",
            "couille",
            "couilles",
            "couillu",
            "enculer",
            "niquer",
            "nique ta mère",
            "pisser",
            "putain",
            "pute",
            "roubignoles",
            "ta gueule",
            "turlutte",
            "zob"
        );
        $message = str_ireplace($wordsToCheck, "&#129412 &#129412 &#129412 &#129412", $message);
        return $message;
    }
}
