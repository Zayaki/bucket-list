<?php

namespace App\Controller;

use App\Repository\WishRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/', name: 'main_')]
class MainController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function home(WishRepository $wishRepository): Response
    {
        {
            $wishs = $wishRepository->findBy(['isPublished' => 1], ['dateCreated' => 'DESC'],50);
            return $this->render("main/home.html.twig", ["wishs" => $wishs]);
        }
    }

    #[Route('/about', name: 'about')]
    public function aboutUs(): Response
    {
        {
            return $this->render("main/about.html.twig");
        }
    }
}