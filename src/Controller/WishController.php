<?php

namespace App\Controller;

use App\Entity\Wish;
use App\Form\WishType;
use App\Repository\SerieRepository;
use App\Repository\WishRepository;
use App\Utils\Censurator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/wish', name: 'wish_')]
class WishController extends AbstractController
{
    #[Route('/list', name: 'list')]
    public function list(WishRepository $wishRepository): Response
    {
        $wishs = $wishRepository->findByPseudo($this->getUser()->getUserIdentifier());
        return $this->render('wish/list.html.twig', ["wishs" => $wishs]);
    }

    #[Route('/detail/{id}', name: 'detail', requirements: ["id" => "\d+"])]
    public function detail(int $id, WishRepository $wishRepository): Response
    {
        $wish = $wishRepository->find($id);

        if (!$wish){
            throw $this->createNotFoundException("Oops ! wish doesn't exist!");
        }
        return $this->render('wish/detail.html.twig', ["wish" => $wish]);
    }

    #[Route('/create', name: 'create')]
    public function create(Request $request, WishRepository $wishRepository, Censurator $censurator): Response
    {
        $wish = new Wish();
        $wish->setAuthor($this->getUser()->getPseudo());

        $wishForm = $this->createForm(WishType::class,$wish);
        $wishForm->handleRequest($request);

        if ($wishForm->isSubmitted() && $wishForm->isValid()){
            $wish->setIsPublished(true);
            $wish->setDateCreated(new \DateTime());

            $purifyDescription = $censurator->purify($wish->getDescription());
            $wish->setDescription($purifyDescription);

            $wishRepository->add($wish, true);

            $this->addFlash("success", "Well Done ! You created a new wish !");

            return $this->redirectToRoute("wish_detail", ['id' => $wish->getId()]);
        }

        return $this->render('wish/create.html.twig',[
            "wishForm" => $wishForm->createView()
        ]);
    }
}
